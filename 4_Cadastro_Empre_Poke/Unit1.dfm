object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 370
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 480
    Top = 183
    Width = 209
    Height = 176
    OnClick = Image1Click
  end
  object Panel2: TPanel
    Left = 62
    Top = 64
    Width = 202
    Height = 241
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 288
    Top = 24
    Width = 225
    Height = 153
    TabOrder = 2
    object lb_id: TLabel
      Left = 16
      Top = 16
      Width = 65
      Height = 13
      Caption = 'Identificador:'
    end
    object lb_nome: TLabel
      Left = 16
      Top = 54
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object lb_nivel: TLabel
      Left = 16
      Top = 96
      Width = 27
      Height = 13
      Caption = 'Nivel:'
    end
    object DBText1: TDBText
      Left = 96
      Top = 16
      Width = 105
      Height = 17
      DataField = 'id'
      DataSource = DataModule3.DataSourceEmpresas
    end
    object DBText2: TDBText
      Left = 96
      Top = 54
      Width = 105
      Height = 17
      DataField = 'nome'
      DataSource = DataModule3.DataSourceEmpresas
    end
    object DBText3: TDBText
      Left = 96
      Top = 96
      Width = 105
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule3.DataSourceEmpresas
    end
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 24
    Top = 24
    Width = 218
    Height = 264
    KeyField = 'id'
    ListField = 'nome'
    ListSource = DataModule3.DataSourceEmpresas
    TabOrder = 0
  end
  object DBNavigator1: TDBNavigator
    Left = 24
    Top = 328
    Width = 240
    Height = 25
    TabOrder = 1
  end
  object btn_editar: TButton
    Left = 336
    Top = 232
    Width = 97
    Height = 25
    Caption = 'Editar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object btn_inserir: TButton
    Left = 336
    Top = 263
    Width = 97
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btn_inserirClick
  end
  object btn_deletar: TButton
    Left = 336
    Top = 294
    Width = 97
    Height = 25
    Caption = 'Deletar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
end
