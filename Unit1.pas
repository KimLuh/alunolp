unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    lb_nome: TLabel;
    ed_nome: TEdit;
    lb_profes: TLabel;
    lb_idade: TLabel;
    ed_idade: TEdit;
    lb_celular: TLabel;
    ed_celular: TEdit;
    lb_sexo: TLabel;
    ed_sexo: TEdit;
    btn_enviar: TButton;
    btn_inserir: TButton;
    btn_remover: TButton;
    lb_cpf: TLabel;
    ed_cpf: TEdit;
    btn_salvar: TButton;
    Profs_Box: TListBox;
    btn_mostrar: TButton;
    btn_gravar: TButton;
    btn_limpar: TButton;
    procedure btn_enviarClick(Sender: TObject);
    procedure btn_removerClick(Sender: TObject);
    procedure btn_salvarClick(Sender: TObject);
    procedure btn_inserirClick(Sender: TObject);
    procedure btn_mostrarClick(Sender: TObject);
    procedure btn_gravarClick(Sender: TObject);
    procedure btn_limparClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TProfs = class(TObject)
    nome : string;
    idade : integer;
    sexo :string;
    celular : string;
    cpf : string;
  end;


var
  Form1: TForm1;
  arquivo: TextFile;
  profs_cedup : TProfs;

implementation

{$R *.dfm}

procedure TForm1.btn_enviarClick(Sender: TObject);
begin
Profs_Box.Items.Add(ed_nome.Text);
Profs_Box.Items.Add(ed_sexo.Text);
Profs_Box.Items.Add(ed_idade.Text);
Profs_Box.Items.Add(ed_cpf.Text);
Profs_Box.Items.Add(ed_celular.Text);
end;

procedure TForm1.btn_removerClick(Sender: TObject);
begin
Profs_Box.DeleteSelected;
end;

procedure TForm1.btn_salvarClick(Sender: TObject);
begin
Profs_Box.Items.SaveToFile('D:\04 -Informática 3\352_(2019)\LP\trabalho_luana\salvar_aqui.txt');
end;

procedure TForm1.btn_inserirClick(Sender: TObject);
begin
profs_cedup := TProfs.Create;

profs_cedup.nome := ed_nome.Text;
profs_cedup.idade := strtoint(ed_idade.Text);
profs_cedup.sexo := ed_sexo.Text;
profs_cedup.celular := ed_celular.Text;
profs_cedup.cpf := ed_cpf.Text;

Profs_Box.Items.AddObject(profs_cedup.nome, profs_cedup);
end;

procedure TForm1.btn_mostrarClick(Sender: TObject);
var nome : string;
professores : TProfs;
begin
  AssignFile(arquivo, 'mostrar.txt');
  Reset(arquivo);

  professores := TProfs.Create();

  while not EOF(arquivo) do
    begin
       Readln(arquivo, professores.nome);
       Readln(arquivo, professores.idade);
       Readln(arquivo, professores.sexo);
       Readln(arquivo, professores.celular);
       Readln(arquivo, professores.cpf);

       Profs_Box.Items.AddObject(professores.nome, professores);
    end
end;

procedure TForm1.btn_gravarClick(Sender: TObject);

var arquivo : TextFile;

begin
profs_cedup := TProfs(Profs_Box.Items[Profs_Box.ItemIndex]);

//ShowMessage(profs_cedup.nome);
//ShowMessage(profs_cedup.idade);
//ShowMessage(profs_cedup.sexo);
//ShowMessage(profs_cedup.celular);
//ShowMessage(profs_cedup.cpf);

//ShowMessage('Gravou!');


AssignFile(arquivo, 'mostrar.txt');
Rewrite(arquivo);
Writeln(arquivo, profs_cedup.nome);
Writeln(arquivo, inttostr(profs_cedup.idade));
Writeln(arquivo, profs_cedup.sexo);
Writeln(arquivo, profs_cedup.celular);
Writeln(arquivo, profs_cedup.cpf);

CloseFile(arquivo);
end;

procedure TForm1.btn_limparClick(Sender: TObject);
begin
Profs_Box.Clear;
end;

end.
