object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Empresas'
  ClientHeight = 395
  ClientWidth = 374
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 64
    Top = 24
    Width = 257
    Height = 273
    TabOrder = 0
    object lb_nome: TLabel
      Left = 24
      Top = 48
      Width = 49
      Height = 21
      Caption = 'Nome:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object lb_grupo: TLabel
      Left = 24
      Top = 104
      Width = 53
      Height = 21
      Caption = 'Grupo:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object lb_nivel: TLabel
      Left = 24
      Top = 168
      Width = 42
      Height = 21
      Caption = 'Nivel:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
    object ed_nome: TEdit
      Left = 112
      Top = 50
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 112
      Top = 104
      Width = 121
      Height = 21
      KeyField = 'id'
      ListField = 'nome'
      ListSource = DataModule3.DataSourceGrupo
      TabOrder = 1
    end
    object ed_nivel: TEdit
      Left = 112
      Top = 170
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object btn_cancelar: TButton
    Left = 88
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object btn_salvar: TButton
    Left = 222
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = btn_salvarClick
  end
end
