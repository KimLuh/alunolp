object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 473
  ClientWidth = 458
  Color = clCream
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 32
    Width = 289
    Height = 433
    Color = clInactiveCaption
    ParentBackground = False
    TabOrder = 0
    object lb_nome: TLabel
      Left = 32
      Top = 20
      Width = 45
      Height = 22
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb_profissao: TLabel
      Left = 32
      Top = 92
      Width = 67
      Height = 22
      Caption = 'Profissao'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb_nivel: TLabel
      Left = 32
      Top = 164
      Width = 40
      Height = 22
      Caption = 'Nivel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb_ataque: TLabel
      Left = 32
      Top = 228
      Width = 53
      Height = 22
      Caption = 'Ataque'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb_defesa: TLabel
      Left = 32
      Top = 292
      Width = 50
      Height = 22
      Caption = 'Defesa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb_vida: TLabel
      Left = 32
      Top = 364
      Width = 35
      Height = 22
      Caption = 'Vida'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Palatino Linotype'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ed_nome: TEdit
      Left = 32
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object ed_profissao: TEdit
      Left = 32
      Top = 120
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object ed_nivel: TEdit
      Left = 32
      Top = 192
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object ed_ataque: TEdit
      Left = 32
      Top = 256
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object ed_defesa: TEdit
      Left = 32
      Top = 320
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object ed_vida: TEdit
      Left = 32
      Top = 392
      Width = 121
      Height = 21
      TabOrder = 5
    end
  end
  object lb_verificar: TButton
    Left = 344
    Top = 69
    Width = 89
    Height = 25
    Caption = 'Verificar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Palatino Linotype'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object lb_inserir: TButton
    Left = 344
    Top = 133
    Width = 89
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Palatino Linotype'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = lb_inserirClick
  end
  object lb_voltar: TButton
    Left = 8
    Top = 1
    Width = 75
    Height = 25
    Caption = 'Voltar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Palatino Linotype'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = lb_voltarClick
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 112
  end
end
