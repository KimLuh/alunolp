object Form1: TForm1
  Left = 192
  Top = 125
  Width = 928
  Height = 480
  Caption = 'Form1'
  Color = clGradientActiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lb_nome: TLabel
    Left = 200
    Top = 104
    Width = 126
    Height = 21
    Caption = 'Nome Completo:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object lb_profes: TLabel
    Left = 280
    Top = 40
    Width = 360
    Height = 34
    Caption = 'PROFESSORES CADASTRO:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object lb_idade: TLabel
    Left = 200
    Top = 144
    Width = 45
    Height = 21
    Caption = 'Idade:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object lb_celular: TLabel
    Left = 192
    Top = 184
    Width = 56
    Height = 21
    Caption = 'Celular:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object lb_sexo: TLabel
    Left = 456
    Top = 144
    Width = 40
    Height = 21
    Caption = 'Sexo:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object lb_cpf: TLabel
    Left = 456
    Top = 184
    Width = 36
    Height = 21
    Caption = 'CPF:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
  end
  object ed_nome: TEdit
    Left = 336
    Top = 104
    Width = 193
    Height = 21
    TabOrder = 0
  end
  object ed_idade: TEdit
    Left = 256
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object ed_celular: TEdit
    Left = 256
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object ed_sexo: TEdit
    Left = 512
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object btn_enviar: TButton
    Left = 256
    Top = 320
    Width = 83
    Height = 25
    Caption = 'Enviar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = btn_enviarClick
  end
  object btn_inserir: TButton
    Left = 368
    Top = 320
    Width = 81
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btn_inserirClick
  end
  object btn_remover: TButton
    Left = 480
    Top = 320
    Width = 97
    Height = 25
    Caption = 'Remover'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btn_removerClick
  end
  object ed_cpf: TEdit
    Left = 512
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 7
  end
  object btn_salvar: TButton
    Left = 32
    Top = 392
    Width = 137
    Height = 25
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = btn_salvarClick
  end
  object Profs_Box: TListBox
    Left = 616
    Top = 224
    Width = 281
    Height = 201
    ItemHeight = 13
    TabOrder = 9
  end
  object btn_mostrar: TButton
    Left = 208
    Top = 392
    Width = 113
    Height = 25
    Caption = 'Mostrar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = btn_mostrarClick
  end
  object btn_gravar: TButton
    Left = 352
    Top = 392
    Width = 113
    Height = 25
    Caption = 'Gravar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = btn_gravarClick
  end
  object btn_limpar: TButton
    Left = 496
    Top = 392
    Width = 91
    Height = 25
    Caption = 'Limpar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'MV Boli'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = btn_limparClick
  end
end
