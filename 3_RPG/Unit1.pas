unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    LBoxPerso: TListBox;
    Atualizar: TButton;
    NetHTTPClient1: TNetHTTPClient;
    Button1: TButton;
    procedure AtualizarClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure LBoxPersoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    TPersonagem = class (TObject)
    nome : string;
    profissao : string;
    nivel : integer;
    ataque : integer;
    defesa : integer;
    vida : integer;
    end;

var
  Form1: TForm1;
  Personagem : TPersonagem;

implementation

{$R *.dfm}

uses Unit2;

procedure TForm1.AtualizarClick(Sender: TObject);
var
conteudo : string;
ListPersona : TArray<String>;
stringPersona : string;
lista : string;
ListPersonag: TArray<System.string>;
begin
LBoxPerso.Clear;

 conteudo := NetHTTPClient1.Get('https://venson.net.br/ws/personagens/').ContentAsString;
 ListPersona := conteudo.Split(['&']);

 for stringPersona in ListPersona do
   begin
     ListPersonag := stringPersona.Split([';']);

          Personagem := TPersonagem.Create();
          Personagem.nome := ListPersonag[0];
          Personagem.profissao := ListPersonag[1];
          Personagem.nivel := ListPersonag[2].ToInteger;
          Personagem.ataque := ListPersonag[3].ToInteger;
          Personagem.defesa := ListPersonag[4].ToInteger;
          Personagem.vida := ListPersonag[5].ToInteger;
          LBoxPerso.Items.AddObject(Personagem.nome, Personagem);

   end;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
Form2.Show();
end;

procedure TForm1.LBoxPersoClick(Sender: TObject);
begin
Personagem:= TPersonagem (LBoxPerso.Items.Objects[LBoxPerso.ItemIndex]);
ShowMessage('Seu personagem � o ' + Personagem.nome);
end;

end.
